$(function () {
  AOS.init();
});

var itemPerPage = 24;
var key;
const url = "https://www.googleapis.com/books/v1/volumes?q=";
// const weburl = "http://localhost:8000/boogle/"
const weburl = "http://wewanttotalkaboutjoni.herokuapp.com/boogle/";

$body = $("body");

const body = document.body;
const themeSwitch = document.getElementById("myonoffswitch");

themeSwitch.onclick = () => {
  if (body.classList.contains("light")) {
    body.classList.replace("light", "dark");
  } else {
    body.classList.replace("dark", "light");
  }
};

const commentModal = document.querySelector(".modal");

function ajaxStart() {
  $body.addClass("loading");
}
function ajaxStop() {
  $body.removeClass("loading");
}
const apiKey = "256d520d";
const myQuery = "Bob the builder";
$(document).ready(function () {
  $("button").on("click", function () {
    $.ajax({
      url: "/static/data/myfile.txt",
      dataType: "text",
      success: function (response) {
        var myTable = "";
        var data = response.split("\n");
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            var kue = data[i][0];
            var harga = data[i][1];
            var jumlah = data[i][2];
            var gambar = data[i][3];
            $("#kue").append(
              `
        <tr>
        <td>${kue}</td>
        </tr>
        <tr>
        <td>${harga}</td>
        </tr>
        <tr>
        <td>${jumlah}</td>
        </tr>
        <tr>
        <td><img src=${gambar} alt=”kue”></td>
        </tr>
        `
            );
          }
        }
      },
    });
  });

  //   ajaxStart();
  $.ajax({
    type: "POST",
    url: `http://www.omdbapi.com/?t=${myQuery}&apikey=${apiKey}`,
    success: function (res) {
      console.log(res);
      const title = res["Title"];
      const genre = res["Genre"];
      const plot = res["Plot"];
      const ratings = res["Ratings"];
      var ratingsContainer;
      var response = res["Response"];
      if (response) {
        for (var i = 0; i < ratings.length; i++) {
          ratingsContainer.append(
            `<p>${ratings[i].Source}:${ratings[i].Value}</p>`
          );
        }
        $("result").append(
          `<div>
          <table>
          <tr>
          <td>
          <p><strong>Judul(Tahun)</strong></p>
          </td>
          <td>
          <p>${title}</p>
          </td>
          </tr>
          <tr>
          <td>
          <p><strong>Jenis</strong></p>
          </td>
          <td>
          <p>${genre}</p>
          </td>
          </tr>
          <tr>
          <td>
          <p><strong>Alur</strong></p>
          </td>
          <td>
          <p>${plot}</p>
          </td>
          </tr>
          <tr>
          <td>
          <p><strong>Rating</strong></p>
          </td>
          <td>
          ${ratingsContainer}
          </td>
          </tr>
          </table>
          </div>`
        );
      } else {
        $("result").append(`<h1>Film tidak ditemukan!</h1>`);
      }
    },
  });
});

function getTop() {
  ajaxStart();
  $.ajax({
    type: "POST",
    url: weburl,
    success: function (res) {
      var books = JSON.parse(res);
      $(".bookmodal").empty();
      for (var i = books.length - 1; i >= 0; i--) {
        $(".bookmodal").append(
          `<div class='book' data-aos="flip-left" data-aos-duration="1000">
                    <div class='book-img'>
                        <p class="like-count">${books[i].likes}</p>
                        <div class="like">
                            <svg height="30px" viewBox="0 -20 464 464" width="30px" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="m340 0c-44.773438.00390625-86.066406 24.164062-108 63.199219-21.933594-39.035157-63.226562-63.19531275-108-63.199219-68.480469 0-124 63.519531-124 132 0 172 232 292 232 292s232-120 232-292c0-68.480469-55.519531-132-124-132zm0 0" />
                                <path
                                    d="m32 132c0-63.359375 47.550781-122.359375 108.894531-130.847656-5.597656-.769532-11.242187-1.15625025-16.894531-1.152344-68.480469 0-124 63.519531-124 132 0 172 232 292 232 292s6-3.113281 16-8.992188c-52.414062-30.824218-216-138.558593-216-283.007812zm0 0"
                                    fill="#ff5023" />
                            </svg>
                        </div>
                        <a href="https://books.google.co.id/books?id=${books[i].id}&hl=en&source=gbs_api" target='blank'>
                            <img src="${books[i].image_url}" alt="">
                        </a>
                    </div>
                </div>`
        );
      }
      $(".modal").addClass("open");
      ajaxStop();
    },
  });
}

function search(keyword) {
  ajaxStart();
  $.ajax({
    url: url + keyword,
    success: function (result) {
      key = keyword;
      $(".books").empty();
      for (i = 0; i < result.items.length; i++) {
        $(".books").append(bookMarkup(result.items[i]));
      }
      $(".like").on("click", function () {
        $(this).children().attr("fill", "#ff6243");
        var id = $(this).parent().parent()[0].id.split("~");
        $.ajax({
          type: "POST",
          url: weburl + "like/",
          dataType: "json",
          data: {
            id: id[0],
            title: id[1],
            img: id[2],
          },
          success: function (e) {
            notif(id[1], e);
          },
        });
      });
      ajaxStop();
    },
  });
}

function notif(e, likes) {
  $(".notification").append(`Successfully liked ${e} (${likes} likes!)`);
  $(".notification").addClass("active");
  setTimeout(() => {
    $(".notification").removeClass("active");
    $(".notification").empty();
  }, 2000);
}

// function getCookie(c_name)
// {
//     if (document.cookie.length > 0)
//     {
//         c_start = document.cookie.indexOf(c_name + "=");
//         if (c_start != -1)
//         {
//             c_start = c_start + c_name.length + 1;
//             c_end = document.cookie.indexOf(";", c_start);
//             if (c_end == -1) c_end = document.cookie.length;
//             return unescape(document.cookie.substring(c_start,c_end));
//         }
//     }
//     return "";
//  }

function bookMarkup(book) {
  var { infoLink, imageLinks, title } = book.volumeInfo;
  var id = infoLink.split("?")[1].split("&")[0].split("=")[1];
  return `<div class='book' id="${id}~${title}~${
    imageLinks ? imageLinks.thumbnail : "noImg"
  }" data-aos="flip-left" data-aos-duration="1000">
            <div class='book-img'>
                <div class="like">
                    <svg height="30px" viewBox="0 -20 464 464" width="30px" xmlns="http://www.w3.org/2000/svg"><path d="m340 0c-44.773438.00390625-86.066406 24.164062-108 63.199219-21.933594-39.035157-63.226562-63.19531275-108-63.199219-68.480469 0-124 63.519531-124 132 0 172 232 292 232 292s232-120 232-292c0-68.480469-55.519531-132-124-132zm0 0"/><path d="m32 132c0-63.359375 47.550781-122.359375 108.894531-130.847656-5.597656-.769532-11.242187-1.15625025-16.894531-1.152344-68.480469 0-124 63.519531-124 132 0 172 232 292 232 292s6-3.113281 16-8.992188c-52.414062-30.824218-216-138.558593-216-283.007812zm0 0" fill="#ff5023"/>
                    </svg>
                </div>
                <a href="${infoLink}" target='blank'>
                    <img src="${
                      imageLinks ? imageLinks.thumbnail : null
                    }" alt=''>
                </a></div>
            <div class='content'><p class='nothing'>${title}</p></div>`;
}
