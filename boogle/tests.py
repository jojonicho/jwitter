from django.test import TestCase
from selenium import webdriver


class Story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        response = Client().get("/boogle/")
        self.assertEqual(response.status_code, 200)

    def test_story9_url_is_exist(self):
        response = Client().post("/boogle/")
        self.assertEqual(response.status_code, 200)

    def test_story9_using_index_func(self):
        found = resolve("/boogle/")
        self.assertEqual(found.func, index)

    def test_story9_url_like_is_exist(self):
        response = Client().post(
            "/boogle/like/", {"id": "abcd", "title": "joni book", "img": "blabla"}
        )
        self.assertEqual(response.status_code, 200)

    def test_story9_using_like_func(self):
        found = resolve("/boogle/like/")
        self.assertEqual(found.func, like)

    def test_story9_book_model(self):
        Book.objects.create(title="re:zero", likes=1000)
        count = Book.objects.all().count()
        self.assertEqual(count, 1)

    def test_story9_increment_like_book_model(self):
        Book.objects.create(title="re:zero", likes=1000)
        book = Book.objects.get(title="re:zero")
        book.likes += 1
        book.save()
        self.assertEqual(Book.objects.get(title="re:zero").likes, 1001)


class Story9FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")

        self.selenium = webdriver.Chrome(
            "./chromedriver", chrome_options=chrome_options
        )
        super(Story9FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story9FunctionalTest, self).tearDown()

    def test_story9_url(self):
        selenium = self.selenium
        selenium.get("http://wewanttotalkaboutjoni.herokuapp.com/boogle/")

    def test_story9_search_then_load_and_display_result(self):
        selenium = self.selenium
        selenium.get("http://wewanttotalkaboutjoni.herokuapp.com/boogle/")
        input = selenium.find_element_by_id("search")
        input.send_keys("re:zero")
        input.send_keys(Keys.RETURN)
        suspense = selenium.find_element_by_css_selector(".suspense")
        selenium.implicitly_wait(1.5)
        book = selenium.find_element_by_css_selector(".book")

    def test_story9_search_using_button_then_load_and_display_result_then_like_rezerovolume1_20_times(
        self,
    ):
        selenium = self.selenium
        selenium.get("http://wewanttotalkaboutjoni.herokuapp.com/boogle/")
        input = selenium.find_element_by_id("search")
        input.send_keys("UhWzCwAAQBAJ")
        search = selenium.find_element_by_css_selector(".enter")
        search.click()
        suspense = selenium.find_element_by_css_selector(".suspense")
        time.sleep(3)
        book = selenium.find_element_by_css_selector(".book")
        like = selenium.find_element_by_css_selector(".like")
        for i in range(20):
            time.sleep(0.2)
            selenium.execute_script("arguments[0].click();", like)

    def test_story9_get_top_liked_books(self):
        selenium = self.selenium
        selenium.get("http://wewanttotalkaboutjoni.herokuapp.com/boogle/")
        top = selenium.find_element_by_css_selector(".accordionheader")
        top.click()
        selenium.implicitly_wait(1.5)
        selenium.find_element_by_css_selector(".modal.open")
