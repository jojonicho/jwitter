from django.urls import path, include
from .views import index, like
from django.conf.urls import url

urlpatterns = [
    path('', index),
    path('like/', like),
]