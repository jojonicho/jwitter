from django.shortcuts import get_object_or_404, render
from boogle.models import Book
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def index(request):
    if request.method == "POST":
        books = list(Book.objects.values()[:5])
        return HttpResponse(json.dumps(books))
    else:
        return render(request, "boogle.html")

@csrf_exempt
def like(request):
    if request.method == 'POST':
        id = request.POST['id']
        title = request.POST['title']
        img = request.POST['img']
        book, created = Book.objects.get_or_create(id=id, title=title, image_url=img)
        book.likes += 1
        book.save()
        return HttpResponse(book.likes)