from django.test import TestCase

# Create your tests here.
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from django.test import Client
from django.urls import resolve

from .views import index, confirm, ran
from .models import Status
from .forms import StatusForm

class Story7UnitTest(TestCase):

    def test_story7_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_story7_confirm_url_is_exist(self):
        response = Client().post('/confirm/', {'name':'test', 'content':"woo"})
        self.assertEqual(response.status_code, 200)

    def test_story7_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_story7_using_confirm_func(self):
        found = resolve('/confirm/')
        self.assertEqual(found.func, confirm)

    def test_model_can_create_new_status(self):
        Status.objects.create(name='joni', content='jonigaya')
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'name':'','content':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['content'],
            ["This field is required."]
        )

    def test_story7_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'name':test, 'content':"woo"})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_change_color(self):
        test = 'Anonymous'
        Status.objects.create(name='joni', content='jonigaya', color="Red")
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

        html_response = response.content.decode('utf8')
        self.assertIn('<div class="status Red">', html_response)


class Story7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_confirmation_page(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/')
        form_button = selenium.find_element_by_css_selector('.add')
        form_button.click()

        name = selenium.find_element_by_id('id_name')
        content = selenium.find_element_by_id('id_content')

        name.send_keys("joni")
        content.send_keys('aahahahahahahh')
        submit = selenium.find_element_by_css_selector('.comment')
        submit.click()

    def test_input_confirmation_page_and_post(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/')
        form_button = selenium.find_element_by_css_selector('.add')
        form_button.click()

        name = selenium.find_element_by_id('id_name')
        content = selenium.find_element_by_id('id_content')

        name.send_keys("joni")
        content.send_keys('aahahahahahahh')
        submit = selenium.find_element_by_css_selector('.comment')
        submit.click()

        confirm = selenium.find_element_by_css_selector('.confirm')
        confirm.click()

        selenium.implicitly_wait(1)
        selenium.find_element_by_css_selector('.status')

    def test_input_confirmation_page_and_post_and_change_color(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/')
        form_button = selenium.find_element_by_css_selector('.add')
        form_button.click()

        name = selenium.find_element_by_id('id_name')
        content = selenium.find_element_by_id('id_content')

        name.send_keys("joni")
        content.send_keys('aahahahahahahh')
        submit = selenium.find_element_by_css_selector('.comment')
        submit.click()

        confirm = selenium.find_element_by_css_selector('.confirm')
        confirm.click()

        selenium.find_element_by_css_selector('.status')

        change = selenium.find_element_by_css_selector('.change')
        change.click()