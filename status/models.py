from django.db import models

# Create your models here.
class Status(models.Model):
    name = models.CharField(max_length=26)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    color = models.CharField(default="", max_length=7)

    def __str__(self):
        return self.name
    objects = models.Manager()
    
    class Meta:
        ordering = ('-created_at',)
        verbose_name_plural = 'Statuses'