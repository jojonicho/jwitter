from django import forms
# from django.utils.timezone import now
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['name', 'content']