from django.urls import path, include
from .views import index, confirm, ran
from django.conf.urls import url
# from rest_framework.routers import DefaultRouter

# app_name = 'story7'

urlpatterns = [
    path('', index),
    path('confirm/', confirm),
    path('randcolor/<id>', ran)
]