from django.shortcuts import render, redirect

# Create your views here.
from django.shortcuts import get_object_or_404, render
from .forms import StatusForm
from .models import Status
from django.db.models import Q
from django.http import HttpResponseRedirect
import random
# Create your views here.

def index(request):
    if(request.method == "POST"):
        name = request.POST['name']
        content = request.POST['content']
        Status.objects.create(name=name, content=content)
        qs = Status.objects.all()[:6]
        context = {
        'queryset' : qs,
        'form' : StatusForm()
        }
        return render(request, "story7.html", context=context)
    else:
        qs = Status.objects.all()[:6]
        context = {
        'queryset' : qs,
        'form' : StatusForm()
        }
        return render(request, "story7.html", context=context)

def confirm(request):
    if(request.method == "POST"):
        name = request.POST['name']
        content = request.POST['content']
        context = {
            'name': name,
            'content': content,
        }
        return render(request, "confirm.html", context=context)

def ran(request, id):
    if(request.method == 'POST'):
        cur = Status.objects.get(id=id)
        colors = ['Red','Green', 'Blue', 'Yellow']
        rand_idx = random.randint(0,3)
        cur.color = colors[rand_idx]
        cur.save()
        return redirect(index)