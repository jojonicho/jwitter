from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls import include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('status.urls'), name='status'),
    path('tierlist/', include('tierlist.urls'), name='tierlist'),
    path('boogle/', include('boogle.urls'), name='boogle'),
    path('totallysafe/', include('jualdatapribadi.urls'), name='data'),
]

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)