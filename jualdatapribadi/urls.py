from django.urls import path, include
from .views import index, signup, login_view
from django.conf.urls import url

app_name = 'jualdatapribadi'

urlpatterns = [
    path('', index, name='index'),
    path('signup/', signup, name='signup'),
    path('login/', login_view, name='login'),
]