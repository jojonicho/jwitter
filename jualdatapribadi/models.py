from django.db import models
import datetime

from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthday = models.DateField(default=datetime.date.today)
    img_url = models.CharField(max_length=255)

    @property
    def full_name(self):
        return self.user.first_name + " " + self.user.last_name

    def __str__(self):
        return self.user.username
    objects = models.Manager()