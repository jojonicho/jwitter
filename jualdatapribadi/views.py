from django.shortcuts import redirect, render, reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import UserProfile
from .forms import UserForm

def index(request):
    username = request.session.get('username')
    try:
        userprof = UserProfile.objects.get(user__username=username)
    except:
        userprof = None
    context = {
        'userprof': userprof
    }
    return render(request, "home.html", context=context)
        
def signup(request):
    context = {
        'form': UserCreationForm
    }
    if request.method == 'POST':
        password1 = request.POST['password1']
        password2 = request.POST['password2']
        if password1 == password2:
            username = request.POST['username']
            user = User.objects.create_user(username=username, password=password1)
            context = {
                'username': user.username,
                'password': password1
            }
            return render(request, "login.html", context=context)
        context['error'] = "Password do not match!"
        return render(request, "signup.html", context=context)
    return render(request, "signup.html", context=context)

def login_view(request):            
    context = {
        'form': UserForm()
    }
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if(user is not None):
            request.session['username'] = username
            login(request, user)
            return redirect(reverse('jualdatapribadi:index'))
        else:
            return render(request, "login.html", context=context)
    return render(request, "login.html", context=context)