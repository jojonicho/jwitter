from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from django.test import Client
from django.urls import resolve
from .views import index, signup, login_view
from django.contrib.auth.models import User
from .models import UserProfile
import time
import random

class Story10UnitTest(TestCase):

    def test_story10_index_is_exist(self):
        response = Client().get('/totallysafe/')
        self.assertEqual(response.status_code, 200)

    def test_story10_signup_is_exist(self):
        response = Client().get('/totallysafe/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('signup.html')

    def test_story10_signup_post_is_exist(self):
        response = Client().post('/totallysafe/signup/', {'username':'fooooo', 'password1':"barbar123", 'password2':"barbar123"})
        self.assertTemplateUsed('signup.html')
        self.assertEqual(response.status_code, 200)

    def test_story10_signup_post_is_exist2(self):
        response = Client().post('/totallysafe/signup/', {'username':'barbar123', 'password1':"barbar123", 'password2':"barbar123"})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('login.html')

    def test_story10_login_is_exist(self):
        response = Client().get('/totallysafe/login/')
        self.assertEqual(response.status_code, 200)

    def test_story10_login_post_is_exist(self):
        response = Client().post('/totallysafe/login/', {'username':'joni', 'password':"joni"})
        self.assertEqual(response.status_code, 200)
    
    def test_story10_index_function(self):
        found = resolve('/totallysafe/')
        self.assertEqual(found.func, index)

    def test_story10_signup_function(self):
        found = resolve('/totallysafe/signup/')
        self.assertEqual(found.func, signup)

    def test_story10_login_function(self):
        found = resolve('/totallysafe/login/')
        self.assertEqual(found.func, login_view)

    def test_story10_model(self):
        User.objects.create(username='joni', password='passwordjoni')
        count = User.objects.all().count()
        self.assertEqual(count, 1)

    def test_custom_model(self):
        joni = User.objects.create(username='joni', password='passwordjoni')
        UserProfile.objects.create(user=joni, img_url="https://i.imgur.com/7lIcAP5.gif.gif")
        count = UserProfile.objects.all().count()
        self.assertEqual(count, 1)

class Story10FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story10FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story10FunctionalTest, self).tearDown()

    def test_home(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/totallysafe/')
        title = selenium.find_element_by_css_selector('.title > a > h1')
        anon = title.get_attribute('innerHTML') == "Hello, stranger"
        self.assertTrue(anon)

    def test_signup_then_login_then_check_cookie_persists_then_logout_eject_cookie(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/totallysafe/signup/')
        # selenium.implicitly_wait(1)
        time.sleep(3)
        name = selenium.find_element_by_id('id_username')
        pass1 = selenium.find_element_by_id('id_password1')
        pass2 = selenium.find_element_by_id('id_password2')
        pre = str(random.randint(0,9))
        r = str(random.randint(1000000000, 9999999999))
        name.send_keys(pre+"joni"+r) 
        pass1.send_keys('ahaha123')
        pass2.send_keys('ahaha123')
        submit = selenium.find_element_by_css_selector('.nothing')
        submit.click()

        time.sleep(2)
        login = selenium.find_element_by_css_selector('.nothing')
        login.click()

        selenium.implicitly_wait(1)
        title = selenium.find_element_by_css_selector('.title > a > h1')
        joni = title.get_attribute('innerHTML') == "Welcome back, "+pre+"joni"+r
        self.assertTrue(joni)

        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/totallysafe/')
        selenium.implicitly_wait(1)
        title = selenium.find_element_by_css_selector('.title > a > h1')
        joni = title.get_attribute('innerHTML') == "Welcome back, "+pre+"joni"+r
        self.assertTrue(joni)

        logout = selenium.find_element_by_css_selector('.accordionheader')
        logout.click()

        title = selenium.find_element_by_css_selector('.title > a > h1')
        anon = title.get_attribute('innerHTML') == "Hello, stranger"
        self.assertTrue(anon)