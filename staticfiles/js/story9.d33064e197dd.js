var itemPerPage=12;
var key;
const url = "https://www.googleapis.com/books/v1/volumes?q=";

$body = $("body");

const body = document.body;
const themeSwitch = document.getElementById("myonoffswitch");

themeSwitch.onclick = () => {
    if(body.classList.contains('light')) {
        body.classList.replace("light", "dark");
    } else {
        body.classList.replace("dark", "light");
    }
}

$(document).ready(function(){
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();

    $(".accordion > .info").hide();
    $(".accordion").map( (e) => {
        $("#" + e + " > .info").hide();
        $("#" + e + " > .header > .accordionheader").click(() => {
            $("#" + e + " > .info").slideToggle();
        });
    })
})

$('#search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search(event.target.value);
    }
});

function ajaxStart() { $body.addClass("loading"); }
function ajaxStop() { $body.removeClass("loading"); }  

function search(keyword){
    ajaxStart();
    $.ajax({
        url: url+keyword,
        success: function(result){
            key = keyword
            $("#sortable")[0].innerHTML = ""
            for(i=0; i<result.items.length; i++){
                $("#sortable")[0].innerHTML += bookMarkup(result.items[i]);
            }
            ajaxStop();
        }
    })
}


function bookMarkup(book){
    var {infoLink, imageLinks, title} = book.volumeInfo;
    return (
        "<div class='book'>"+
        "<a href="+infoLink+" target='blank'>"+
        "<div class='book-img'><img src="+(imageLinks ? imageLinks.thumbnail : null )+" alt=''></div></a>"+
        "<div class='content'>"+title+""+
        "</div>"
    )
}