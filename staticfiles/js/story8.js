const body = document.body;
const themeSwitch = document.getElementById("myonoffswitch");

themeSwitch.onclick = () => {
    if(body.classList.contains('light')) {
        body.classList.replace("light", "dark");
    } else {
        body.classList.replace("dark", "light");
    }
}

$(document).ready(function(){
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();

    $(".accordion > .info").hide();

    $(".accordion").map( (e) => {
        $("#" + e + " > .info").hide();
        $("#" + e + " > .header > .textheader").click(() => {
        // $("#" + e).toggleClass("active");
            $("#" + e + " > .info").slideToggle();
        });
    })

    $(".Green").click(function() {
        var el = $(this).parent().parent().parent();
        if (el.not(':last-child'))
            el.prev().before(el);
    });

    $(".Red").click(function(){
        var el = $(this).parent().parent().parent();
        if (el.not(':first-child'))         
            el.next().after(el);
    });
    
});