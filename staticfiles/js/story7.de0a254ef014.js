const commentModal = document.querySelector('.blur');
const addComment = document.querySelector('.add');

addComment.addEventListener('click', () => {
    commentModal.classList.add('open');
})

commentModal.addEventListener('click', (e) => {
    if (e.target.classList.contains('blur')) {
        commentModal.classList.remove('open');
    }
})