$(function() {
    AOS.init();
});

themeSwitch.onclick = () => {
    if(body.classList.contains('light')) {
        body.classList.replace("light", "dark");
    } else {
        body.classList.replace("dark", "light");
    }
}