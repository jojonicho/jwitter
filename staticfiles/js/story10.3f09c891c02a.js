$(function() {
    AOS.init();
});

$body = $("body");

const body = document.body;
const themeSwitch = document.getElementById("myonoffswitch");

themeSwitch.onclick = () => {
    if(body.classList.contains('light')) {
        body.classList.replace("light", "dark");
    } else {
        body.classList.replace("dark", "light");
    }
}