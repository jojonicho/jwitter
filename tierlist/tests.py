from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from django.test import Client
from django.urls import resolve
from .views import index
from .models import About

class Story8UnitTest(TestCase):

    def test_story8_url_is_exist(self):
        response = Client().get('/tierlist/')
        self.assertEqual(response.status_code, 200)
    
    def test_story8_index_function(self):
        found = resolve('/tierlist/')
        self.assertEqual(found.func, index)

    def test_story8_model(self):
        About.objects.create(title="hello", description="i am awesome")
        count = About.objects.all().count()
        self.assertEqual(count, 1)

class Story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_story8_url(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/tierlist/')

    def test_story8_accordion(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/tierlist/')
        accordion = selenium.find_element_by_css_selector('.accordion')
        accordion.click()
        selenium.find_element_by_css_selector('.info')

    def test_story8_accordion_up(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/tierlist/')
        accordion_up = selenium.find_element_by_css_selector('.Green')
        accordion_up.click()

    def test_story8_accordion_down(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/tierlist/')
        accordion_down = selenium.find_element_by_css_selector('.Red')
        accordion_down.click()

    def test_story8_toggle_theme(self):
        selenium = self.selenium
        selenium.get('http://wewanttotalkaboutjoni.herokuapp.com/tierlist/')
        theme_switch = selenium.find_element_by_css_selector('.onoffswitch')
        theme_switch.click()
        selenium.find_element_by_css_selector('.light')
        theme_switch.click()
        selenium.find_element_by_css_selector('.dark')
