from django.urls import path, include
from .views import index
from django.conf.urls import url

urlpatterns = [
    path('', index),
]