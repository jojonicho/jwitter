from django.shortcuts import render
from .models import About

# Create your views here.
def index(request):
    qs = About.objects.all()
    context = {
        'queryset' : qs
    }
    return render(request, 'tierlist.html', context=context)