from django.db import models

# Create your models here.
class About(models.Model):
    title = models.CharField(max_length=40)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ('created_at',)
        verbose_name_plural = 'Abouts'

    def __str__(self):
        return self.title
    objects = models.Manager()